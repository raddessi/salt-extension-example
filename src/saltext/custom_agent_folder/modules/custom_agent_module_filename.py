'''
Custom Agent lets you write a file on the minion
'''

import os


__DEFAULT_ROOT = '/tmp'
_root = __DEFAULT_ROOT


def __init__(opts):
    global _root
    _root = opts.get("direct_root", __DEFAULT_ROOT)


def __virtual__():
    if os.path.exists(_root):
        return True
    else:
        return False


####################################################################################################################
#  Module functions
####################################################################################################################

def write_hello_world(contents, directory_path=_root):
    '''
    Writes a file to the specified path

    :param contents: the contents of the file to be written
    :param directory_path: (optional) the path that the file will be written to
                           if excluded - uses /tmp
    :return: a descriptive string
    '''
    filename = 'custom_module.out'
    file_path = f'{directory_path}/{filename}'
    with open(file_path, 'w') as file:
        file.write(contents)
    return f'File written to {file_path}.'



####################################################################################################################
#  Internal utility functions
####################################################################################################################

def _internal_sample_function():
    return True
