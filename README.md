
# Salt Extension Example

## Description
This is an example that demonstrates the use of `poetry` to build the extension and package it into a wheel. It also includes a `.gitlab-ci.yml` file that builds the Python package and publishes the artifacts to the repo's GitLab Python Package Registry.

## Status
Tested and confirmed working using Salt 3006.3 (onedir) running on a Rocky Linux 9.1 VM.

## Building the Python Package
### Manually
```
➜  ~ git clone https://gitlab.com/eliezerlp/salt-extension-example.git
Cloning into 'salt-extension-example'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 2.79 KiB | 476.00 KiB/s, done.
➜  ~ cd salt-extension-example/
➜  salt-extension-example git:(main) python3 -m venv venv
➜  salt-extension-example git:(main) source venv/bin/activate
(venv) ➜  salt-extension-example git:(main) pip install poetry
[...]
Successfully installed SecretStorage-3.3.3 attrs-23.1.0 build-0.10.0 cachecontrol-0.13.1 certifi-2023.7.22 cffi-1.16.0 charset-normalizer-3.3.0 cleo-2.0.1 crashtest-0.4.1 cryptography-41.0.4 distlib-0.3.7 dulwich-0.21.6 filelock-3.12.4 idna-3.4 importlib-metadata-6.8.0 installer-0.7.0 jaraco.classes-3.3.0 jeepney-0.8.0 jsonschema-4.17.3 keyring-24.2.0 more-itertools-10.1.0 msgpack-1.0.7 packaging-23.2 pexpect-4.8.0 pkginfo-1.9.6 platformdirs-3.11.0 poetry-1.6.1 poetry-core-1.7.0 poetry-plugin-export-1.5.0 ptyprocess-0.7.0 pycparser-2.21 pyproject-hooks-1.0.0 pyrsistent-0.19.3 rapidfuzz-2.15.1 requests-2.31.0 requests-toolbelt-1.0.0 shellingham-1.5.3 tomli-2.0.1 tomlkit-0.12.1 trove-classifiers-2023.9.19 urllib3-2.0.6 virtualenv-20.24.5 zipp-3.17.0
(venv) ➜  salt-extension-example git:(main) poetry build
Building custom-agent (0.1.0)
  - Building sdist
  - Built custom_agent-0.1.0.tar.gz
  - Building wheel
  - Built custom_agent-0.1.0-py3-none-any.whl
(venv) ➜  salt-extension-example git:(main) ls dist/
custom_agent-0.1.0-py3-none-any.whl  custom_agent-0.1.0.tar.gz
(venv) ➜  salt-extension-example git:(main) 
```

After building, you can find the build artifacts in the `dist` folder:
```
(venv) ➜  salt-extension-example git:(main) tree -I 'venv'
.
├── README.md
├── dist
│   ├── custom_agent-0.1.0-py3-none-any.whl
│   └── custom_agent-0.1.0.tar.gz
├── pyproject.toml
└── src
    └── saltext
        └── custom_agent_folder
            ├── __init__.py
            ├── loader.py
            └── modules
                ├── __init__.py
                └── custom_agent_module_filename.py

5 directories, 8 files
(venv) ➜  salt-extension-example git:(main)
```

### Using GitLab CI + GitLab Python Package Registry
To kick off the build CI job, create a new tag via git or via the GitLab web UI. Ensure that the tag is unique, otherwise pushing to the GitLab Python Package Registry will fail with an HTTP 400 error.

See `.gitlab-ci.yml` file in this repo for more info on the build process.



## Installing the Salt Extension into Salt onedir (relenv) directory
### Manual Installation
Copy the `custom_agent-0.1.0-py3-none-any.whl` wheel file to the Minion that you'd like to install the extension on.

Then run `salt-pip` to install the package:
```
[root@minion ~]# salt-pip install /path/to/module/custom_agent-0.1.0-py3-none-any.whl --root-user-action=ignore --disable-pip-version-check
Processing /vagrant/custom_agent-0.1.0-py3-none-any.whl
Requirement already satisfied: psutil<6.0.0,>=5.8.0 in /opt/saltstack/salt/lib/python3.10/site-packages (from custom-agent==0.1.0) (5.8.0)
Installing collected packages: custom-agent
Successfully installed custom-agent-0.1.0
[root@minion ~]#
```

### Using GitLab Python Package Registry
Ensure the package is present in the GitLab Python Package Registry (see above for more info).

Then run `salt-pip` to install the package:
```
[root@minion ~]# salt-pip install custom-agent --root-user-action=ignore --disable-pip-version-check --index-url https://gitlab.com/api/v4/projects/50994764/packages/pypi/simple --no-deps
Looking in indexes: https://gitlab.com/api/v4/projects/50994764/packages/pypi/simple
Collecting custom-agent
  Downloading https://gitlab.com/api/v4/projects/50994764/packages/pypi/files/a7a35aebfc9e278a709391a3ebe839bd59fc81186fdbfe6b2aef0d96cc3765cf/custom_agent-0.1.0-py3-none-any.whl (4.2 kB)
Installing collected packages: custom-agent
Successfully installed custom-agent-0.1.0
[root@minion ~]# 
```
Be sure to use the correct Project ID (`50994764` in this example) in the the `--index-url`.

## Calling the Salt Module
```
[root@minion ~]# salt-call custom_agent_module_filename.write_hello_world "Text to write"
local:
    File written to /tmp/custom_module.out.
[root@minion ~]#
```

## Other
- You can leverage Salt (States, Pillars, etc...) to distribute and manage the package on desired Minion's respective Salt in-built Python environments.
- Recall that in onedir releases, anything installed using `pip.installed`  by default lands in Salt's onedir (relenv) Python environment.


## Useful links and acknowledgment
- [saltstack/salt-extension: Tool to simplify the creation of a new salt extension](https://github.com/saltstack/salt-extension)
- [Salt Extensions Overview by s0undt3ch (Pedro Algarvio) - YouTube](https://www.youtube.com/watch?v=hhomJkwxK3Q)
- [saltstack/saltext-presentation: Presentation Test Extension](https://github.com/saltstack/saltext-presentation/tree/master)

## License
[WTFPL](https://en.wikipedia.org/wiki/WTFPL)
